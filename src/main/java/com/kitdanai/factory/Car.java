/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.factory;

/**
 *
 * @author ADMIN
 */
public class Car {
    protected int numberOfWheels;
    protected String color;
    protected String nameOfCar;
    public Car(int numberOfWheels,String color,String nameOfCar){
        System.out.println("Car built");
        this.numberOfWheels = numberOfWheels;
        this.color = color;
        this.nameOfCar = nameOfCar;
    }
    public void information(){
        System.out.println("Number Of Wheels: "+this.numberOfWheels+", Color: "+
                this.color+", Name of car: "+this.nameOfCar);
    }
    public void startOfCar(){
        System.out.println("Start Engine");
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public String getColor() {
        return color;
    }

    public String getNameOfCar() {
        return nameOfCar;
    }
}
