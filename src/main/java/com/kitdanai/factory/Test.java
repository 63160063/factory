/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.factory;

/**
 *
 * @author ADMIN
 */
public class Test {
    public static void main(String[] args) {
        Car car = new Car(4,"black","camry");
        car.information();
        car.startOfCar();
        CarA carA = new CarA("red","colola");
        carA.information();
        carA.startOfCar();
         CarB carB = new CarB("blue","yaris");
        carB.information();
        carB.startOfCar();
    }
}
